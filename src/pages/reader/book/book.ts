import {Component} from '@angular/core';
import {NavController, Platform, PopoverController, Events, NavParams, LoadingController} from 'ionic-angular';
import {TocPage} from '../toc/toc';
import {SettingsPage} from '../settings/settings';
import {BookProvider, Value} from "../../../providers/db/book";
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {HomePage} from "../../home/home";
import {Settings, SettingsProvider} from "../../../providers/db/settings";

declare var ePub: any;

@Component({
	selector: 'page-book',
	templateUrl: 'book.html'
})
export class BookPage {

	slider;
	isIos: boolean;
	value: Value;
	book: any;
	pageTitle: string;
	setting: Settings = new Settings();
	showToolbars: boolean = true;
	bgColor: any;
	toolbarColor: string = 'light';
	private _ready = new BehaviorSubject<boolean>(false);

	constructor(public navCtrl: NavController,
				public platform: Platform,
				public popoverCtrl: PopoverController,
				public events: Events,
				public navParams: NavParams,
				private book_db: BookProvider,
				public loadingCtrl: LoadingController,
				private settong_db: SettingsProvider) {

		this.loaderSpinner().then();

		this.value = this.navParams.get('book');


		this.platform.ready().then(() => {

			//back-button action !!!
			this.platform.registerBackButtonAction(() => {
				this._ready.next(true);
				this.navCtrl.setRoot(HomePage);
			});

			this.isIos = this.platform.is('ios');

			this.value.current_page = isNaN(+this.value.current_page) ? 1 : +this.value.current_page;


			// load book
			this.book = ePub(this.value.location);

			this.setSettings();

			this._updateTotalPages();

			// load toc and then update pagetitle
			this.book.getToc().then(toc => {
				this._updatePageTitle();
			});

			// if page changes
			this.book.on('book:pageChanged', (location) => {
				console.log('on book:pageChanged', location);
				this._updateCurrentPage();
				this._updatePageTitle();
			});

			// subscribe to events coming from other pages
			this._subscribeToEvents();

			this.book.getMetadata().then(meta => {
				this.value.lable = meta.bookTitle;
				this.value.author = meta.creator;
				this.value.description = meta.description;
				this.book_db.add_book(this.value).then(() => {
					// alert(this.value.toString());
				});
			});
		});
	}


	watchSlider(){
		this.value.current_page = this.slider;
		this.book.gotoPage(+this.value.current_page);
		this.book_db.set_value(this.value.id, this.book_db.value.current_page, +this.value.current_page).then();
	}
	private setSettings () {
		this.settong_db.get_settings().then(settings => {
			if (settings != null){
				this.setting = settings;
				this.book.setStyle("font-size", settings.font_size);
				this.book.setStyle("background-color", settings.background);
				this.book.setStyle("color", settings.color);
				this.book.setStyle("font-family", settings.font_family);
			} else {
				this.setting.font_family = this.book.settings.styles['font-family'];
				this.setting.background = this.book.settings.styles['background-color'];
				this.setting.font_size = this.book.settings.styles['font-size'];
				this.setting.color = this.book.settings.styles['color'];
				this.saveSettings();
			}
		}).catch()
	}

	private saveSettings(){
		this.settong_db.add_settings(this.setting);
	}

	private loaderSpinner(): Promise<any> {
		return new Promise(resolve => {
			const loading = this.loadingCtrl.create({
				content: 'Please wait...'
			});

			loading.present();

			this.isReady().then(() => {
				loading.dismiss();
				resolve();
			});
		});
	}

	private isReady() {
		return new Promise((resolve, reject) => {
			if (this._ready.getValue()) {
				resolve();
			}
			else {
				this._ready.subscribe((ready) => {
					if (ready) {
						resolve();
					}
				});
			}
		})
	}

	ionViewDidLoad() {
		// render book
		this.book.renderTo("book"); // TODO We should work with ready somehow here I think
	}

	_subscribeToEvents() {
		// toc: go to selected chapter
		this.events.subscribe('select:toc', (content) => {
			this.book.goto(content.href);
		});

		// settings: change background color
		this.events.subscribe('select:background-color', (bgColor) => {
			this.setting.background = bgColor;
			this.book.setStyle("background-color", bgColor);
			this.bgColor = bgColor;
			// adapt toolbar color to background color
			if (bgColor == 'rgb(255, 255, 255)' || bgColor == 'rgb(249, 241, 228)') { // TODO don't hardcode color values, use some metadata
				this.toolbarColor = 'light';
			}
			else {
				this.toolbarColor = 'dark';
			}
			this.saveSettings();
		});

		// settings: change color
		this.events.subscribe('select:color', (color) => {
			this.setting.color = color;
			this.book.setStyle("color", color);
			this.saveSettings();
		});

		// settings: change font
		this.events.subscribe('select:font-family', (family) => {
			this.setting.font_family = family;
			this.book.setStyle("font-family", family);
			this._updateTotalPages();
		});

		// settings: change font size
		this.events.subscribe('select:font-size', (size) => {
			this.setting.font_size = size;
			this.book.setStyle("font-size", size);
			this._updateTotalPages();
		});

	}

	_updateCurrentPage() {
		// Source: https://github.com/futurepress/epub.js/wiki/Tips-and-Tricks#generating-and-getting-page-numbers (bottom)
		let currentLocation = this.book.getCurrentLocationCfi();
		this.value.current_page = +this.book.pagination.pageFromCfi(currentLocation);
		this.slider = this.value.current_page;
		this.book_db.set_value(this.value.id, this.book_db.value.current_page, +this.value.current_page).then();
	}

	_updateTotalPages() {
		// TODO: cancel prior pagination promise
		// Source: https://github.com/futurepress/epub.js/wiki/Tips-and-Tricks#generating-and-getting-page-numbers
		this.book.generatePagination().then(() => {
			this.value.total_pages = +this.book.pagination.totalPages;
			this.book_db.set_value(this.value.id, this.book_db.value.total_pages, +this.value.total_pages).then();
			this.saveSettings();
			this.book.gotoPage(+this.value.current_page);
			this._ready.next(true);
		}).catch(error => {
			console.log('_updateTotalPages error = ', error);
		});
	}

	_updatePageTitle() {
		let bookTitle = this.book.metadata.bookTitle;
		let pageTitle = bookTitle; // default to book title
		if (this.book.toc) {
			// Use chapter name
			let chapter = this.book.toc.filter(obj => obj.href == this.book.currentChapter.href)[0]; // TODO What does this code do?
			pageTitle = chapter ? chapter.label : bookTitle; // fallback to book title again
		}
		console.log('_updatePageTitle title =', pageTitle);
		this.pageTitle = pageTitle;
	}

	// Navigation

	prev() {
		if (this.value.current_page == 2) { // TODO Why this special case here?
			this.book.gotoPage(1);
		} else {
			this.book.prevPage();
		}
	}

	next() {
		this.book.nextPage();
	}

	toc(ev) {
		console.log('toc');
		let popover = this.popoverCtrl.create(TocPage, {
			toc: this.book.toc
		});
		popover.present({ev});
	}

	settings(ev) {
		let popover = this.popoverCtrl.create(SettingsPage, {
			backgroundColor: this.book.settings.styles['background-color'],
			fontFamily: this.book.settings.styles['font-family'],
			fontSize: this.book.settings.styles['font-size'],
		});
		popover.present({ev});
	}


	// Touchlayer

	toggleToolbars() {
		this.showToolbars = !this.showToolbars;
	}

	changePage(event) {
		console.log('changePage', event);
		if (event.velocityX < 0) { // TODO Best way to do this?
			this.next();
		}
		else {
			this.prev();
		}
	}

}
