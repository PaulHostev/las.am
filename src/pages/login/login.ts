import {Component} from '@angular/core';
import {AlertController, LoadingController, NavController, Platform} from 'ionic-angular';
import {LoginProvider} from "../../providers/login/login";
import {Profile, Token, UserProvider} from "../../providers/db/user";
import {TabsPage} from "../main/tabs/tabs";

@Component({
	selector: 'page-login',
	templateUrl: 'login.html',
})
export class LoginPage {

	private loading: any;
	public login = {
		username: '', //PaulHost
		password: '' //overly123
	};

	private old_login;

	constructor(public navCtrl: NavController,
				public provider: LoginProvider,
				private alertCtrl: AlertController,
				private user_db: UserProvider,
				private loadingCtl: LoadingController,
				private platform: Platform) {
		this.platform.registerBackButtonAction(() => {
			this.loading.dismiss();
		});
		this.user_db.get_login().then(o => {
			this.login = o;
			this.old_login = o;
		})
	}

	dialog() {
		this.loading = this.loadingCtl.create({ content: "Please wait..."});
		this.loading.present();
		this.provider.logIn(this.login).then(data => {
			if (data.status == 200) {
				let token = new Token();
				let _data = JSON.parse(data.data);
				token.sessname = _data['session_name'];
				token.sessid = _data['sessid'];
				token.token = _data['token'];
				this.set_profile(_data['user']).then().catch();
				this.user_db.set_token(token).then(() => {
					this.loading.dismiss();
					if (this.login == this.old_login) {
						this.navCtrl.setRoot(TabsPage);
					} else {
						this.user_db.clean_login().then(() => {
							this.user_db.set_login(this.login).then(() => {
								this.navCtrl.setRoot(TabsPage);
							}).catch(e => {
								alert(e);
							});
						});
					}
				}).catch(e => {
					this.loading.dismiss();
					alert('');
				});
			} else {
				this.loading.dismiss();
				this._alert('Retry!', '');
			}
		})
		.catch(error => {
			this.loading.dismiss();
			console.log('login http err', error);
		});

	}

	_alert(a, b) {
		this.alertCtrl.create({
			title: a,
			subTitle: b,
			buttons: ['lol']
		}).present();
	}

	goToHome() {
		this.navCtrl.push(TabsPage);
	}

	set_profile(json): Promise<any> {
		return new Promise(resolve => {
			let profile: Profile = new Profile();
			profile.uid = json.uid;
			profile.timezone = json.timezone;
			profile.language = json.language;
			profile.init = json.init;
			profile.picture = json.picture.url;
			profile.user_first_name = json.field_user_first_name.und[0].value;
			profile.user_last_name = json.field_user_last_name.und[0].value;
			this.user_db.set_profile(profile).then(() => {
				resolve();
			});
		}).catch();

	}
}
