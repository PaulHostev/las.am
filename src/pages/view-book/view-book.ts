import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import {Book} from "../../providers/book-loader/book-loader";

@Component({
  selector: 'page-view-book',
  templateUrl: 'view-book.html',
})
export class ViewBookPage {

  book: Book;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.book = this.navParams.get('book');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ViewBookPage');
  }

}
