import {BookPage} from '../reader/book/book';
import {Component} from '@angular/core';
import {LoadingController, NavController, NavParams, Platform} from 'ionic-angular';
import {BookProvider, Value} from "../../providers/db/book";
import {FileChooser} from "@ionic-native/file-chooser";
import {Book, BookLoaderProvider} from "../../providers/book-loader/book-loader";

@Component({
	selector: 'page-home',
	templateUrl: 'home.html',
})
export class HomePage {

	next: number = 1;
	loading;
	isIOS: boolean;
	saved: Array<Value>;
	books: Array<Book>;
	isOnline: boolean = false;

	constructor(private platform: Platform,
				public navCtrl: NavController,
				public navParams: NavParams,
				private book_db: BookProvider,
				private fileChooser: FileChooser,
				private my_books: BookLoaderProvider,
				private loadingCtl: LoadingController) {
		this.saved = [];
		this.isIOS = this.platform.is('ios');
		this.platform.registerBackButtonAction(() => {
			this.loading.dismiss();
		});
		this.book_db.get_books().then((data: Array<Value>) => {
			this.saved = data;
		});
		this.my_books.load_my_books().then((data: Array<Book>) => {
			this.books = data;
			this.isOnline = data.length > 0;
		}).catch(e => {alert(e)});
	}

	show(book) {
		this.navCtrl.push(BookPage, {
			book: book
		});
	}

	download(book: Book){
		this.loading = this.loadingCtl.create({
			content: 'Please wait...'
		});
		for (let item of this.saved){
			if (item.id == book.nid){
				this.show(item);
				return;
			}
		}
		this.loading.present();
		this.my_books.download_book(book).then((value: Value) => {
			this.loading.dismiss();
			this.saved.push(value);
			this.show(value);
		});
	}

	openFile() {
		this.fileChooser.open().then(_path => {
			let path = decodeURIComponent(_path);
			for (let i = 0; i< this.saved.length; i++) {
				if (this.saved[i].location === path) {
					this.show(this.saved[i]);
					return;
				}
			}
			let _book: Value = new Value();
			_book.id = this.saved.length + 1;
			_book.location = path;
			this.show(_book);
		});
	}

	doInfinite(infiniteScroll){
		setTimeout(() => {
			this.my_books.load_my_next(this.next).then((next: Array<Book>) => {
				this.books = this.books.concat(next);
				this.next++;
			});
			infiniteScroll.complete();
		}, 500);
	}
}
