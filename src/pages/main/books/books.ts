import {Component} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import {ViewBookPage} from "../../view-book/view-book";
import {Book, BookLoaderProvider} from "../../../providers/book-loader/book-loader";

@Component({
	selector: 'page-books',
	templateUrl: 'books.html',
})
export class BooksPage {

	books: Array<Book>;
	next: number = 1;

	constructor(public navCtrl: NavController,
				public navParams: NavParams,
				private loader: BookLoaderProvider) {
		this.loader.load_books().then((list: Array<Book>) => {
			this.books = list;
		});
	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad BooksPage');
	}

	doInfinite(infiniteScroll){

			setTimeout(() => {
				this.loader.load_next(this.next).then((next: Array<Book>) => {
					this.books = this.books.concat(next);
					this.next++;
				});
				infiniteScroll.complete();
			}, 500);
	}

	show(book){
		this.navCtrl.push(ViewBookPage, {
			book: book
		})
	}

}
