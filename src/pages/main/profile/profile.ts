import {Component} from '@angular/core';
import {LoadingController, NavController, NavParams, Platform} from 'ionic-angular';
import {Profile, UserProvider} from "../../../providers/db/user";
import {LoginPage} from "../../login/login";

@Component({
	selector: 'page-profile',
	templateUrl: 'profile.html',
})
export class ProfilePage {

	profile: Profile;
	private loading;

	constructor(public navCtrl: NavController,
				public navParams: NavParams,
				private platform: Platform,
				private user: UserProvider,
				private db_user: UserProvider,
				private loadingCtl: LoadingController) {
		this.platform.registerBackButtonAction(() => {
			this.loading.dismiss();
		});
		this.user.get_prifile().then((pro: Profile) => {
			this.profile = pro;
		}).catch();
		this.loading = this.loadingCtl.create({content: "Please weit..."});
	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad ProfilePage');
	}

	log_out() {
		this.loading.present();
		this.db_user.log_out().then(() => {
			this.loading.dismiss();
			this.navCtrl.setRoot(LoginPage);
		}).catch(() => {
			alert('last')
		});
	}
}
