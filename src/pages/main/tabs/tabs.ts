import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {BooksPage} from "../books/books";
import {ProfilePage} from "../profile/profile";
import {HomePage} from "../../home/home";

@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html'
})
export class TabsPage {

  homeRoot = BooksPage;
  booksRoot =  HomePage;
  profileRoot = ProfilePage;

  constructor(public navCtrl: NavController) {}

}
