import {Injectable} from '@angular/core';
import 'rxjs/add/operator/map';
import {HTTP} from "@ionic-native/http";
import {Token, UserProvider} from "../db/user";
import {File} from "@ionic-native/file";
import {FileTransfer, FileTransferObject, FileUploadOptions} from "@ionic-native/file-transfer";
import {BookProvider, Value} from "../db/book";

@Injectable()
export class BookLoaderProvider {

	URL: string = 'https://las.am/api/econtent';
	next: string = '?page=';
	Purchased: string = 'https://las.am/api/my';
	fileTransfer: FileTransferObject;

	constructor(public http: HTTP,
				private user: UserProvider,
				private book_db: BookProvider,
				private file: File,
				private transfer: FileTransfer) {
		this.fileTransfer = this.transfer.create();
	}

	get_(url, session_name, sessid, token): Promise<any> {
		return this.http.get(url, '', {
			'Content-Type': 'application/json',
			'Cookie': session_name + '=' + sessid,
			'X-CSRF-Token': token
		});
	}

	get_books(url): Promise<any> {
		return new Promise(resolve => {
			this.user.get_token().then((t: Token) => {
				if (t.sessname && t.sessid && t.token) {
					this.get_(url, t.sessname, t.sessid, t.token).then(data => {
						if (data.status < 400) {
							resolve(JSON.parse(data.data));
						}
					});
				}
			});
		});
	}

	load_my_books(): Promise<Array<Book>> {
		return new Promise(resolve => {
			this.user.get_token().then((t: Token) => {
				if (t.sessname && t.sessid && t.token) {
					this.get_(this.Purchased, t.sessname, t.sessid, t.token).then(data => {
						if (data.status < 400) {
							this.parse_json_porchised(data).then(list => {
								resolve(list);
							});
						}
					});
				}
			});
		});
	}

	load_my_next(i: number): Promise<Array<Book>> {
		return new Promise(resolve => {
			this.user.get_token().then((t: Token) => {
				if (t.sessname && t.sessid && t.token) {
					let url = this.Purchased + this.next + i;
					this.get_(url, t.sessname, t.sessid, t.token).then(data => {
						this.parse_json_porchised(data).then(books => {
							resolve(books);
						});
					});
				}
			});
		});
	}

	load_next(i: number): Promise<Array<Book>> {
		return new Promise(resolve => {
			this.get_books(this.URL + this.next + i).then(json => {
				this.parse_json_econtent(json).then(books => {
					resolve(books);
				});
			})
		});
	}

	load_books(): Promise<Array<Book>> {
		return new Promise(resolve => {
			this.get_books(this.URL).then(json => {
				this.parse_json_econtent(json).then(list => {
					resolve(list);
				})
			});
		});
	}

	download_book(book: Book): Promise<Value> {
		return new Promise(resolve => {
			let value: Value = new Value();
			value.id = book.nid;
			value.location = this.file.dataDirectory + book.nid + '.epub';
			this.user.get_token().then((t: Token) => {
				if (t.sessname && t.sessid && t.token) {
					let options: FileUploadOptions = {
						headers: {
							'Content-Type': 'application/json',
							'Cookie': t.sessname + '=' + t.sessid,
							'X-CSRF-Token': t.token
						}
					};
					this.fileTransfer.download(book.path, encodeURI(value.location), true, options).then((entry) => {
						this.download_cover(book.image, book.nid, options).then(img => {
							value.cover = img;
							value.lable = book.title;
							this.book_db.add_book(value).then(() => {
								resolve(value);
							});
						})
					}, (error) => {
						alert("error => " + error);
					});
				}
			});

		});
	}

	download_cover(url, name, options: FileUploadOptions): Promise<string> {
		return new Promise(resolve => {
			let path: string = this.file.dataDirectory + name + '.jpg';
			this.fileTransfer.download(url, path, true, options).then(data => {
				resolve(decodeURIComponent(path));
			})
		});
	}

	parse_json_porchised(data): Promise<Array<Book>> {
		return new Promise(resolve => {
			let books: Array<Book> = [];
			let book: Book;
			let json = JSON.parse(data.data);
			let nodes = json.nodes;
			for (let item of nodes) {
				let node = item.node;
				if (node) {
					book = new Book();
					book.nid = +node.nid;
					book.title = node.title;
					book.image = node.image;
					book.image_src_mdpi = node.image_src_mdpi;
					book.image_src_xhdpi = node.image_src_xhdpi;
					book.image_src_xxhdpi = node.image_src_xxhdpi;
					book.image_src_banner = node.image_src_banner;
					book.path = node.uri;
					book.year = node.year;
					book.author = node.author;
				}
				books.push(book);
			}
			resolve(books);
		});
	}

	parse_json_econtent(json): Promise<Array<Book>> {
		return new Promise(resolve => {
			let books: Array<Book> = [];
			let book: Book;
			let nodes = json.nodes;
			for (let item of nodes) {
				let node = item.node;
				book = new Book();
				book.nid = +node.nid;
				book.title = node.title;
				book.image = node.image;
				book.image_src_mdpi = node.image_src_mdpi;
				book.image_src_xhdpi = node.image_src_xhdpi;
				book.image_src_xxhdpi = node.image_src_xxhdpi;
				book.image_src_banner = node.image_src_banner;
				book.price = node.price;
				book.path = node.path;
				book.description = node.description;
				book.year = node.year;
				book.author = node.author;
				books.push(book);
			}
			resolve(books);
		});
	}
}

export class Book {
	nid: number = 0;
	title: string = 'title';
	image: string = 'image';
	image_src_mdpi: string = 'image_src_mdpi';
	image_src_xhdpi: string = 'image_src_xhdpi';
	image_src_xxhdpi: string = 'image_src_xxhdpi';
	image_src_banner: string = 'image_src_banner';
	price: string = 'price';
	path: string = 'path';
	description: string = 'description';
	year: string = 'year';
	author: string = 'author';
}
