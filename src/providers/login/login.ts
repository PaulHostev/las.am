import {Injectable} from '@angular/core';
import 'rxjs/Rx';
import {HTTP} from '@ionic-native/http'

@Injectable()
export class LoginProvider {

	private URL: string = 'https://las.am/api/user/login.json';
	private connect: string = 'https://las.am/api/system/connect.json';
	private log_out: string = 'https://las.am/api/user/logout.json';

	constructor(public http: HTTP) {
	}

	logIn(data): Promise<any> {
		return this.http.post(this.URL, data, {'Content-Type': 'application/json'});
	}

	isLogIn(session_name: string, sessid: string, token: string): Promise<any> {
		return this.http.post(this.connect, "", new Headers({
			'Content-Type': 'application/json',
			'Cookie': session_name + '=' + sessid,
			'X-CSRF-Token': token
		}));
	}

	logOut(/*session_name: string, sessid: string, */token: string): Promise<any> {
		return this.http.post(this.log_out, "", new Headers({
			'Content-Type': 'application/json',
			// 'Cookie': session_name + '=' + sessid,
			'X-CSRF-Token': token
		}));
	}
}
