import {Injectable} from '@angular/core';
import {DbProvider} from "./db";

export class Token {
	id: number = 1;
	sessname: string = 'sessname';
	sessid: string = 'sessid';
	token: string = 'token';

	toString(): string {
		return this.sessname + '\n' + this.sessid + '\n' + this.token;
	}
}

@Injectable()
export class UserProvider {
	token: Token = new Token();
	profile: Profile = new Profile();
	login_table: string = 'login';
	token_table: string = 'token';
	profile_table: string = 'profile';

	constructor(private _db: DbProvider) {
		this._db.init_db();
		this.init().then();
	}

	private init(): Promise<any> {
		return new Promise(resolve => {
			this.init_token().then(() => {
				this.init_login().then(() => {
					this.init_profile().then();
					resolve()
				});
			});
		});
	}

	private init_token(): Promise<any> {
		return this._db.query(`CREATE TABLE IF NOT EXISTS ${this.token_table}(id INTEGER PRIMARY KEY, ${this.token.sessname} VARCHAR, ${this.token.sessid} VARCHAR, ${this.token.token} VARCHAR);`, []);
	}

	private init_login(): Promise<any> {
		return this._db.query(`CREATE TABLE IF NOT EXISTS ${this.login_table}(id INTEGER PRIMARY KEY, username VARCHAR, password VARCHAR);`, []);
	}

	private init_profile(): Promise<any> {
		return this._db.query(`CREATE TABLE IF NOT EXISTS ${this.profile_table}(${this.profile.uid} INTEGER PRIMARY KEY, ${this.profile.timezone} VARCHAR, ${this.profile.language} VARCHAR, ${this.profile.picture} VARCHAR, ${this.profile.init} VARCHAR, ${this.profile.user_first_name} VARCHAR, ${this.profile.user_last_name} VARCHAR);`, []);
	}

	get_login(): Promise<any> {
		return new Promise(resolve => {
			this._db.query(`SELECT * FROM ${this.login_table};`, []).then(data => {
				let item;
				let username = '';
				let password = '';
				if (data.res.rows.length) {
					item = data.res.rows.item(data.res.rows.length - 1);
					username = item.username;
					password = item.password;
				}
				resolve({username, password});
			});
		});
	}

	set_login(o) {
		return new Promise(resolve => {
			this._db.query(`INSERT OR REPLACE INTO ${this.login_table} VALUES (${1},'${o.username}','${o.password}');`, []).then(() => {
				resolve();
			});
		});
	}

	get_token(): Promise<Token> {
		return new Promise(resolve => {
			this._db.query(`SELECT * FROM ${this.token_table};`, []).then(data => {
				let token = new Token();
				token.sessname = '';
				token.sessid = '';
				token.token = '';
				if (data.res.rows.length) {
					let item = data.res.rows.item(data.res.rows.length - 1);
					token.sessname = item.sessname;
					token.sessid = item.sessid;
					token.token = item.token;
				}
				resolve(token);
			});
		});
	}

	set_token(token: Token): Promise<any> {
		return new Promise(resolve => {
			this._db.query(`INSERT OR REPLACE INTO ${this.token_table} VALUES (${1}, '${token.sessname}', '${token.sessid}', '${token.token}');`, []).then(() => {
				resolve();
			});
		});
	}

	get_prifile(): Promise<Profile> {
		return new Promise(resolve => {
			this._db.query(`SELECT * FROM ${this.profile_table};`, []).then(data => {
				let profile: Profile = new Profile();
				if (data.res.rows.length) {
					let item = data.res.rows.item(data.res.rows.length - 1);
					profile.uid = item.uid;
					profile.timezone = item.timezone;
					profile.language = item.language;
					profile.init = item.init;
					profile.picture = item.picture;
					profile.user_first_name = item.user_first_name;
					profile.user_last_name = item.user_last_name;
				}
				resolve(profile);
			});
		});
	}

	set_profile(profile: Profile): Promise<any> {
		return new Promise(resolve => {
			this._db.query(`INSERT OR REPLACE INTO ${this.profile_table} VALUES (${profile.uid}, '${profile.timezone}', '${profile.language}', '${profile.picture}', '${profile.init}', '${profile.user_first_name}', '${profile.user_last_name}');`, []).then(() => {
				resolve();
			});
		});
	}

	log_out(): Promise<any> {
		return new Promise(resolve => {
			//droping all tables
			this.drop_table(this.token_table).then(() => {
				this.drop_table(this.login_table).then(() => {
					this.drop_table(this.profile_table).then(() => {
			//init tables
						this.init().then(() => {
							resolve()
						});
					})
				})
			})
		});
	}

	clean_login(): Promise<any> {
		return new Promise(resolve => {
			this.drop_table(this.login_table).then(() => {
				this.init_login().then(() => {
					resolve();
				})
			});
		});
	}

	clean_token(): Promise<any> {
		return new Promise(resolve => {
			this.drop_table(this.token_table).then(() => {
				this.init_token().then(() => {
					resolve();
				})
			});
		});
	}

	drop_table(table: string): Promise<any> {
		return this._db.query(`DROP TABLE ${table};`, []);
	}
}

export class Profile {
	uid = 'uid';
	timezone = 'timezone';
	language = 'language';
	picture = 'picture';
	init = "init";
	user_first_name = 'user_first_name';
	user_last_name = 'user_last_name';

	toString(): string {
		return `${this.uid}\n${this.timezone}\n${this.language}\n${this.init}\n${this.picture}\n${this.user_first_name}\n${this.user_last_name}`;
	}
}
