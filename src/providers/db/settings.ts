import {Injectable} from '@angular/core';
import 'rxjs/add/operator/map';
import {DbProvider} from "./db";

export class Settings {
	font_size: any = 'font_size';
	background: any = 'background';
	color: any = 'color';
	font_family: any = 'font_family';
}

@Injectable()
export class SettingsProvider {

	private table: string = "settings";
	settings = new Settings();

	constructor(private _db: DbProvider) {
		this._db.init_db();
		this.init_tables();
	}

	private init_tables() {
		this._db.query(`CREATE TABLE IF NOT EXISTS ${this.table}(id INTEGER PRIMARY KEY , ${this.settings.font_size} VARCHAR, ${this.settings.background} VARCHAR, ${this.settings.color} VARCHAR, ${this.settings.font_family} VARCHAR);`, [])
		.then()
		.catch();
	}

	get_settings(): Promise<Settings> {
		return new Promise(resolve => {
			this._db.query(`SELECT * FROM ${this.table}`, []).then(data => {
				if (data.res.rows.length > 0) {
					let item = data.res.rows.item(data.res.rows.length - 1);
					let value = new Settings();
					value.font_size = item.font_size;
					value.background = item.background;
					value.font_family = item.font_family;
					resolve(value);
				}else {
					resolve(null)
				}
			})
		});
	}

	add_settings(settings: Settings): Promise<any> {
		return new Promise(resolve => {
			let q = `INSERT OR REPLACE INTO ${this.table} VALUES (1, '${settings.font_size}', '${settings.background}', '${settings.color}', '${settings.font_family}');`;
			this._db.query(q, []).then(() => {
				resolve();
			}).catch();
		});
	}

	set_setting(id, value, content): Promise<any> {
		return new Promise(resolve => {
			return this._db.query(`UPDATE ${this.table} SET ${value} = '${content}' WHERE id = 1;`, []).then(() => {
				resolve();
			}).catch();
		});
	}

	set_settings(id, setting: Settings): Promise<any> {
		return new Promise(resolve => {
			this._db.query(`UPDATE ${this.table} SET ${this.settings.font_size} = '${setting.font_size}', ${this.settings.background} = '${setting.background}', ${this.settings.color} = '${setting.color}', ${this.settings.font_family} = '${setting.font_family}' WHERE id = 1;`, []).then(() => {
				resolve();
			}).catch();
		});
	}
}
