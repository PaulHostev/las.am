import {Platform} from "ionic-angular";
import {Injectable} from "@angular/core";

const DB_NAME: string = '_las_am_db_';
const win: any = window;

@Injectable()
export class DbProvider {

	private _dbPromise: Promise<any>;

	constructor(public platform: Platform) { }

	init_db() {
		this._dbPromise = new Promise((resolve, reject) => {
			try {
				let _db: any;
				this.platform.ready().then(() => {
					if (this.platform.is('cordova') && win.sqlitePlugin) {
						_db = win.sqlitePlugin.openDatabase({
							name: DB_NAME,
							location: 'default'
						});
					} else {
						alert('Storage: SQLite plugin not installed, falling back to WebSQL. Make sure to install cordova-sqlite-storage in production!');
						_db = win.openDatabase(DB_NAME, '1.1', 'database', 5 * 1024 * 1024);
					}
					resolve(_db);
				});
			} catch (err) {
				reject({err: err});
			}
		});
	}

	query(query: string, params: any[] = []): Promise<any> {
		return new Promise((resolve, reject) => {
			try {
				this._dbPromise.then(db => {
					db.transaction((tx: any) => {
							tx.executeSql(query, params,
								(tx: any, res: any) => resolve({tx: tx, res: res}),
								(tx: any, err: any) => reject({tx: tx, err: err}));
						},
						(err: any) => reject({err: err}));
				});
			} catch (err) {
				reject({err: err});
			}
		});
	}
}
