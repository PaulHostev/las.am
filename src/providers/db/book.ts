import {Injectable} from '@angular/core';
import {DbProvider} from "./db";

export class Value {
	id: number = 0;
	lable: string = "lable";
	cover: string = "cover";
	author: string = "author";
	location: string = "location";
	description: string = "description";
	total_pages: any = "total_page";
	current_page: any = "current_page";
	toString(): string {
		return `${this.id}\n${this.lable}\n${this.author}\n${this.location}\n${this.description}\ncurrent:${this.current_page}\ntotal: ${this.total_pages}`
	}
}

@Injectable()
export class BookProvider {

	private table: string = "book";

	value = new Value();

	constructor(private _db: DbProvider) {
		this._db.init_db();
		this.init_tables();
	}

	private init_tables() {
		this._db.query(`CREATE TABLE IF NOT EXISTS ${this.table}(id INTEGER PRIMARY KEY , ${this.value.lable} VARCHAR, ${this.value.cover} VARCHAR, ${this.value.author} VARCHAR, ${this.value.location} VARCHAR, ${this.value.description} VARCHAR, ${this.value.current_page} INTEGER, ${this.value.total_pages} INTEGER);`, [])
		.then()
		.catch();
	}

	get_book(key): Promise<any> {
		return new Promise(resolve => {
			this._db.query(`SELECT ${key} FROM ${this.table}`, []).then(data => {
				if (data.res.rows.length) resolve(data.res.rows.item(0));
			}).catch();
		});
	}

	get_books(): Promise<Array<Value>> {
		return new Promise(resolve => {
			this._db.query(`SELECT * FROM ${this.table}`, []).then(data => {
				if (data.res.rows.length > 0) {
					let list: Array<Value> = [];
					for (let i = 0; i < data.res.rows.length; i++) {
						let item = data.res.rows.item(i);
						let value = new Value();
						value.id = item.id;
						value.lable = item.lable;
						value.cover = item.cover;
						value.author = item.author;
						value.location = item.location;
						value.description = item.description;
						value.total_pages = item.total_pages;
						value.current_page = item.current_page;
						list.push(value);
					}
					resolve(list);
				}
			})
		});
	}

	add_book(values: Value): Promise<any> {
		return new Promise(resolve => {
			let q = `INSERT OR REPLACE INTO ${this.table} VALUES ('${values.id}', '${values.lable}', '${values.cover}', '${values.author}', '${values.location}', '${values.description}', '${isNaN(+values.current_page) ? 0 : +values.current_page}', '${isNaN(+values.total_pages)? 0 : +values.total_pages}');`;
			this._db.query(q, []).then(() => {
				resolve();
			}).catch();
		});
	}

	set_value(id, value, content): Promise<any> {
		return new Promise(resolve => {
			return this._db.query(`UPDATE ${this.table} SET ${value} = '${content}' WHERE id = ${id};`, []).then(() => {
				resolve();
			}).catch();
		});
	}

	set_values(id, values: Value): Promise<any> {
		return new Promise(resolve => {
			this._db.query(`UPDATE ${this.table} SET ${this.value.lable} = '${values.lable}', ${this.value.cover} = '${values.cover}', ${this.value.author} = '${values.author}', ${this.value.location} = '${values.location}', ${this.value.description} = '${values.description}', ${this.value.total_pages} = '${values.total_pages}', ${this.value.current_page} = '${values.current_page}' WHERE id = ${id};`, []).then(() => {
				resolve();
			}).catch();
		});
	}
}
