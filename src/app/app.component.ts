import {Component} from '@angular/core';
import {LoadingController, Platform} from 'ionic-angular';
import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';

import {LoginPage} from "../pages/login/login";
import {DbProvider} from "../providers/db/db";
// import {BookProvider} from "../providers/db/book";
import {Token, UserProvider} from "../providers/db/user";
import {LoginProvider} from "../providers/login/login";
import {TabsPage} from "../pages/main/tabs/tabs";
import {ScreenOrientation} from "@ionic-native/screen-orientation";

@Component({
	templateUrl: 'app.html'
})
export class MyApp {
	rootPage: any = LoginPage;

	constructor(platform: Platform,
				statusBar: StatusBar,
				splashScreen: SplashScreen,
				private _db: DbProvider,
				private token: UserProvider,
				private login: LoginProvider,
				private loadingCtl: LoadingController,
				private screenOrientation: ScreenOrientation) {
		platform.ready().then(() => {
			statusBar.styleDefault();
			this._db.init_db();
			this.isLogedIn();
			splashScreen.hide();
			this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
		});
	}

	isLogedIn() {
		let loading = this.loadingCtl.create({
			content: 'Please wait...'
		});
		loading.present();
		this.token.get_token().then((t: Token) => {
			if (t.sessname && t.sessid && t.token) {
				this.login.isLogIn(t.sessname, t.sessid, t.token).then(data => {
					loading.dismiss();
					if (data.status < 400) {
						this.rootPage = TabsPage;
					} else {
						this.token.clean_token().then();
					}
				}).catch(e => {
					loading.dismiss();
					this.token.clean_token().then();
				});
			} else {
				loading.dismiss();
				this.token.clean_token().then();
			}
		}).catch(e => {
			alert(e);
		});
	}
}

