import {BrowserModule} from '@angular/platform-browser';
import {ErrorHandler, NgModule} from '@angular/core';
import {IonicApp, IonicErrorHandler, IonicModule} from 'ionic-angular';
import {SplashScreen} from '@ionic-native/splash-screen';
import {StatusBar} from '@ionic-native/status-bar';

import {MyApp} from './app.component';
import {HomePage} from '../pages/home/home';
import {BookPage} from '../pages/reader/book/book';
import {TocPage} from '../pages/reader/toc/toc';
import {SettingsPage} from '../pages/reader/settings/settings';
import {LoginPage} from "../pages/login/login";
import {LoginProvider} from "../providers/login/login";
import {IonicStorageModule} from "@ionic/storage";
import {HTTP} from "@ionic-native/http";
import {DbProvider} from '../providers/db/db';
import {UserProvider} from '../providers/db/user';
import {BookProvider} from '../providers/db/book';
import {FileChooser} from "@ionic-native/file-chooser";
import {BooksPage} from "../pages/main/books/books";
import {ProfilePage} from "../pages/main/profile/profile";
import {TabsPage} from "../pages/main/tabs/tabs";
import {BookLoaderProvider} from "../providers/book-loader/book-loader";
import {ViewBookPage} from "../pages/view-book/view-book";
import {File} from "@ionic-native/file";
import {FileTransfer} from "@ionic-native/file-transfer";
import { SettingsProvider } from '../providers/db/settings';
import {ScreenOrientation} from "@ionic-native/screen-orientation";

@NgModule({
	declarations: [
		MyApp,
		LoginPage,
		HomePage,
		ViewBookPage,
		BookPage,
		BooksPage,
		ProfilePage,
		TabsPage,
		TocPage,
		SettingsPage
	],
	imports: [
		BrowserModule,
		IonicModule.forRoot(MyApp),
		IonicStorageModule.forRoot({
			name: '_las_am_db_',
			driverOrder: ['indexeddb', 'sqlite', 'websql']
		})
	],
	bootstrap: [IonicApp],
	entryComponents: [
		MyApp,
		HomePage,
		BookPage,
		BooksPage,
		ViewBookPage,
		ProfilePage,
		TabsPage,
		TocPage,
		LoginPage,
		SettingsPage
	],
	providers: [
		HTTP,
		File,
		ScreenOrientation,
		FileTransfer,
		StatusBar,
		LoginProvider,
		BookLoaderProvider,
		SplashScreen,
		FileChooser,
		DbProvider,
		UserProvider,
		BookProvider,
		{provide: ErrorHandler, useClass: IonicErrorHandler},
    SettingsProvider
	]
})
export class AppModule {
}
